import 'package:erp_connect_admin/Shared/Utils/Constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:get/get.dart';
import 'package:multilevel_drawer/multilevel_drawer.dart';

class MenuCpn extends StatefulWidget {
  const MenuCpn({Key? key}) : super(key: key);

  @override
  _MenuCpnState createState() => _MenuCpnState();
}

class _MenuCpnState extends State<MenuCpn> {
  final GlobalKey<InnerDrawerState> _innerDrawerKey =
      GlobalKey<InnerDrawerState>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      width: MediaQuery.of(context).size.width * 1, //20.0,
      height: MediaQuery.of(context).size.height * 1,
      child: SafeArea(
        child: Scaffold(
          body: MultiLevelDrawer(
            backgroundColor: APP_COLORS.tdBlue,
            rippleColor: Colors.white,
            subMenuBackgroundColor: Colors.blue.shade100,
            divisionColor: Colors.grey,
            header: SizedBox(
              height: size.height * 0.25,
              child: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(50.0),
                    child: Image.asset(
                      "assets/images/userIcon.png",
                      width: 100,
                      height: 100,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "VanThuan76",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                ],
              )),
            ),
            children: [
              MLMenuItem(
                  leading: const Icon(Icons.home_outlined, size: 25.0),
                  content: const Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: Text("Home", style: TextStyle(fontSize: 16))),
                  onClick: () {
                    Navigator.of(context).pop();
                    Get.toNamed('/home');
                  }),
              MLMenuItem(
                  leading:
                      const Icon(Icons.card_membership_outlined, size: 25.0),
                  trailing: const Icon(Icons.arrow_right),
                  content: const Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: Text("BIDV", style: TextStyle(fontSize: 16))),
                  onClick: () {},
                  subMenuItems: [
                    MLSubmenu(
                        submenuContent: const Text("Transaction"),
                        onClick: () {}),
                    MLSubmenu(
                        submenuContent: const Text("Customer"), onClick: () {}),
                    MLSubmenu(
                        submenuContent: const Text("Account"), onClick: () {})
                  ]),
              MLMenuItem(
                  leading: const Icon(Icons.person_outlined, size: 25.0),
                  trailing: const Icon(Icons.arrow_right),
                  content: const Padding(
                      padding: EdgeInsets.only(left: 15),
                      child:
                          Text("System Admin", style: TextStyle(fontSize: 16))),
                  subMenuItems: [
                    MLSubmenu(
                      onClick: () {
                        Navigator.of(context).pop();
                        Get.toNamed('/admin/system_administrator/user_account');
                        // Navigator.of(context).push(
                        //     MaterialPageRoute(builder: (context) => context));
                      },
                      submenuContent: const Text("User Account"),
                    ),
                    MLSubmenu(
                      onClick: () {

                      },
                      submenuContent: const Text("User Role"),
                    ),
                    MLSubmenu(
                      onClick: () {},
                      submenuContent: const Text("User Permission"),
                    ),
                    MLSubmenu(
                      onClick: () {},
                      submenuContent: const Text("User Log"),
                    ),
                    MLSubmenu(
                      onClick: () {},
                      submenuContent: const Text("Menu"),
                    ),
                  ],
                  onClick: () {}),
            ],
          ),
          backgroundColor: Colors.transparent,
        ),
      ),
    );
  }
}
