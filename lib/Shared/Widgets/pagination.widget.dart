import 'package:flutter/material.dart';
import 'package:pagination_flutter/pagination.dart';

class AppPagination extends StatefulWidget {
  final int selectedPage;
  final Function setStateFn;
  const AppPagination(
      {Key? key, required this.selectedPage, required this.setStateFn})
      : super(key: key);

  @override
  State<AppPagination> createState() => _AppPaginationState();
}

class _AppPaginationState extends State<AppPagination> {
  @override
  Widget build(BuildContext context) {
    return Pagination(
      numOfPages: 10,
      selectedPage: widget.selectedPage,
      pagesVisible: 3,
      spacing: 10,
      onPageChanged: (page) {
        widget.setStateFn(page);
      },
      nextIcon: const Icon(
        Icons.chevron_right_rounded,
        color: Colors.blue,
        size: 20,
      ),
      previousIcon: const Icon(
        Icons.chevron_left_rounded,
        color: Colors.blue,
        size: 20,
      ),
      activeTextStyle: const TextStyle(
        color: Colors.white,
        fontSize: 14,
        fontWeight: FontWeight.w700,
      ),
      activeBtnStyle: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.blue),
        shape: MaterialStateProperty.all(const CircleBorder(
          side: BorderSide(
            color: Colors.blue,
            width: 1,
          ),
        )),
      ),
      inactiveBtnStyle: ButtonStyle(
        elevation: MaterialStateProperty.all(0),
        backgroundColor: MaterialStateProperty.all(Colors.white),
        shape: MaterialStateProperty.all(const CircleBorder(
          side: BorderSide(
            color: Colors.blue,
            width: 1,
          ),
        )),
      ),
      inactiveTextStyle: const TextStyle(
        fontSize: 14,
        color: Colors.blue,
        fontWeight: FontWeight.w700,
      ),
    );
  }
}
