
class Quote {
  Quote({
    required num userId,
    required num id,
    required String title,
  }) {
    _userId = userId;
    _id = id;
    _title = title;
  }

  Quote.fromJson(dynamic json){
    _userId = json['userId'];
    _id = json['id'];
    _title = json['title'];
  }
  late num _userId;
  late num _id;
  late String _title;

  num get userId => _userId;
  num get id => _id;
  String get title => _title;

}
