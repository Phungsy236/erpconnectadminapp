import 'package:flutter/material.dart';

// class LoginDto {
//   final String username;
//   final String password;
//   LoginDto({
//     required this.username,
//     required this.password,
//   });
// }

class TokenDTO{
  late final String access_token;
  late final int expires_in ;
  late final int refresh_expires_in;
  late final String refresh_token ;
  late final String token_type;

  TokenDTO.fromJson(dynamic json){
    access_token = json['access_token'];
    expires_in = json['expires_in'];
    refresh_expires_in = json['refresh_expires_in'];
    refresh_token = json['refresh_token'];
    token_type = json['token_type'];
  }
}