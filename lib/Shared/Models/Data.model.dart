class DataDTO {
  late final String dateAndTime;
  late final String requestId;
  late final String appToken;
  late final String type;
  late final String bankType;
  late final String errorcode;

  late final String errordesc;
  late final String content;

  DataDTO(
      {required this.dateAndTime,
      required this.requestId,
      required this.appToken,
      required this.type,
      required this.bankType,
      required this.errorcode,
      required this.errordesc,
      required this.content});

  DataDTO.fromJson(dynamic json) {
    dateAndTime = json['dateAndTime'];
    requestId = json['requestId'];
    appToken = json['appToken'];
    type = json['type'];
    bankType = json['bankType'];
    errorcode = json['errorcode'];
    errordesc = json['errordesc'];
    content = json['content'];
  }
}
