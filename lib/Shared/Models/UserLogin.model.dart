class UserLoginModel {

  late String userName;
  late String role;

  UserLoginModel({
    required this.userName,
    required this.role,
  });

  UserLoginModel.fromJson(dynamic json) {
    userName = json['userName'];
    role = json['role'];
  }


  UserLoginModel copyWith({
    required String userName,
    required String role,
  }) =>
      UserLoginModel(
        userName: userName ?? this.userName,
        role: role ?? this.role,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['userName'] = userName;
    map['role'] = role;
    return map;
  }
}
