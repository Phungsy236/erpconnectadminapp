class Quote {
  Quote({
    required num userId,
    required num id,
    required String title,
    required String body,
  }) {
    _userId = userId;
    _id = id;
    _title = title;
    _body = body;
  }

  Quote.fromJson(dynamic json) {
    _userId = json['userId'];
    _id = json['id'];
    _title = json['title'];
    _body = json['body'];
  }

  late num _userId;
  late num _id;
  late String _title;
  late String _body;

  Quote copyWith({
    required num userId,
    required num id,
    required String title,
    required String body,
  }) =>
      Quote(
        userId: userId ?? _userId,
        id: id ?? _id,
        title: title ?? _title,
        body: body ?? _body,
      );

  num get userId => _userId;

  num get id => _id;

  String get title => _title;

  String get body => _body;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['userId'] = _userId;
    map['id'] = _id;
    map['title'] = _title;
    map['body'] = _body;
    return map;
  }
}
