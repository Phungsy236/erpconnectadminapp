class UserCreateModel {
  UserCreateModel({
    required this.username,
    required this.email,
    required this.pass,
    required this.attributes,
    required this.groups,
  });


  String username;
  String email;
  String pass;
  Attributes attributes;
  List<String> groups;

  UserCreateModel copyWith({
    required String username,
    required String email,
    required String pass,
    required Attributes attributes,
    required List<String> groups,
  }) =>
      UserCreateModel(
        username: username ?? this.username,
        email: email ?? this.email,
        pass: pass ?? this.pass,
        attributes: attributes ?? this.attributes,
        groups: groups ?? this.groups,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['username'] = username;
    map['email'] = email;
    map['pass'] = pass;
    if (attributes != null) {
      map['attributes'] = attributes.toJson();
    }
    map['groups'] = groups;
    return map;
  }
}

class Attributes {
  Attributes({
    required this.role,
  });

  // Attributes.fromJson(dynamic json) {
  //   role = json['role'];
  // }

  String role;

  Attributes copyWith({
    required String role,
  }) =>
      Attributes(
        role: role ?? this.role,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['role'] = role;
    return map;
  }
}
