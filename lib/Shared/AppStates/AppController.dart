import 'dart:convert';

import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:erp_connect_admin/Shared/Models/Login.dto.dart';
import 'package:erp_connect_admin/Shared/Models/UserLogin.model.dart';
import 'package:erp_connect_admin/Shared/Services/authen.service.dart';
import 'package:get/get.dart';
import 'dart:developer' as devtools show log;

import 'package:get_storage/get_storage.dart';

class AppController extends GetxController {
  var isLogined = RxBool(false);
  final box = GetStorage();

  void checkToken() async {
    var mapKey =  box.read('appKey');
    if(mapKey != null){
       TokenDTO token = TokenDTO.fromJson(mapKey['data']);
       if(token.refresh_token != null){
         try{
           final jwtDecode = JWT.decode(token.refresh_token);
           print(jwtDecode);
           // authService.authenticated({"username":jwtDecode.})
           // var data = await authService.refreshToken();
           // print(data);
         }catch(e){
           print(e);
         }
       }
       isLogined = RxBool(false);
    }
    // quoteService.checkToken();
  }
  void login() {
    isLogined.value = true;
  }

  void logout() {
    isLogined.value = false;
    box.remove('appKey');
  }
}
