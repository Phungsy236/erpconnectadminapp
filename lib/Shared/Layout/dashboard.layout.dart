import 'package:erp_connect_admin/Shared/Widgets/bottomNavbar.widget.dart';
import 'package:flutter/material.dart';

class DashboardLayout extends StatefulWidget {
  final Widget body;
  final int currentIndex;
  final String nameScreen;

  const DashboardLayout(
      {required this.body,
      required this.nameScreen,
      required this.currentIndex,
      Key? key})
      : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _DashboardLayoutState createState() => _DashboardLayoutState();

  Widget buildAppBar(Widget body) {
    return AppBar(
      title: body,
    );
  }
}

class _DashboardLayoutState extends State<DashboardLayout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: const Icon(
              Icons.menu,
              color: Colors.black,
            ),
            onPressed: () {},
          ),
        ),
        extendBodyBehindAppBar: true,
        body: widget.body,
        bottomNavigationBar: const BottomNavBar(index: 0));
  }
}
