import 'package:erp_connect_admin/Shared/Utils/Constant.dart';
import 'package:flutter/material.dart';

class BlankLayout extends StatelessWidget {
  const BlankLayout({super.key});

  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: APP_COLORS.tdBGColor,
      title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [Title(color: APP_COLORS.tdBlack, child: const Text(""))]),
    );
  }

  @override
  Widget build(BuildContext context) {
    throw UnimplementedError();
  }
}
