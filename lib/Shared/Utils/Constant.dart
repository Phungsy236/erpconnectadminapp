// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

class APP_CONSTANT {
  static String BACKEND_URL = 'http://14.248.85.251:8080';
  static String TOKEN_KEY = "secret";
}

class APP_COLORS {
  static Color tdRed = const Color(0xFFDA4040);
  static Color tdBlue = Colors.blue;

  static Color tdBlack = const Color(0xFF3A3A3A);
  static Color tdGrey = const Color(0xFF717171);

  static Color tdBGColor = const Color(0xFFEEEFF5);
}

class APP_ROUTES {
  static const String home = "/home";
  static const String login = "/login";
  static const String error = "/error";
}
