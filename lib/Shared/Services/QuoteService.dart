import 'package:dio/dio.dart';
import 'package:erp_connect_admin/Shared/Models/_example.model.dart';
import 'package:erp_connect_admin/Shared/Services/base.service.dart';

class QuoteService {
  final baseService = BaseService<Quote>();
  Dio dio = getDioInstance(false);
  getDetail() {
    print(this.baseService.baseUrl);
    return baseService.getDetail(
        url: 'albums/1',
        fromJson: (json) => Quote.fromJson(json),
        isAuth: false);
  }

  Future<List<Quote>> getList(int page) async {
    Map<String, num> queryParam = {"_limit": 10, "_page": page};
    final response = await dio.get("http://jsonplaceholder.typicode.com/albums",
        queryParameters: queryParam);
    if (response.statusCode == 200) {
      return response.data.map<Quote>((json) => Quote.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load list ');
    }
  }
}

QuoteService quoteService = QuoteService();
