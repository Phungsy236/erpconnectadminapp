import 'package:erp_connect_admin/Shared/Models/_example.model.dart';
import 'package:erp_connect_admin/Shared/Services/base.service.dart';
import 'package:erp_connect_admin/Shared/Utils/Constant.dart';
import 'package:get_storage/get_storage.dart';

class AuthService {
  final box = GetStorage();
  final baseService = BaseService<Quote>();
  void checkToken() {
    if (box.read(APP_CONSTANT.TOKEN_KEY)) {
    } else {}
  }

   refreshToken(String refresh_token) {
    return baseService.post(url: "/refresh-token" , data : {"refresh_token" : refresh_token} , isAuth: false);
  }

  authenticated(credentials) {
    final String username = credentials['username'];
    final String password = credentials['password'];
    return baseService.post(
        url: 'login',
        isAuth: false,
        data: {"username": username, "password": password});
  }

}

AuthService authService = AuthService();
