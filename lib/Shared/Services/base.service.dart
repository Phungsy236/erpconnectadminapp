
import 'package:dio/dio.dart' ;
import 'package:erp_connect_admin/Shared/Models/Login.dto.dart';

import 'package:erp_connect_admin/Shared/Models/_example.model.dart';
import 'package:erp_connect_admin/Shared/Services/authen.service.dart';
import 'package:erp_connect_admin/Shared/Utils/Constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart' as getx;
// import 'package:get/get_core/src/get_main.dart';
import 'package:get_storage/get_storage.dart';

import '../AppStates/AppController.dart';

Dio getDioInstance(bool isAuth) {
  final appState = getx.Get.find<AppController>();
  Dio dio = Dio();
  final box = GetStorage();
  var mapKey =  box.read('appKey');
  dio.interceptors.add(InterceptorsWrapper(
    onRequest: (RequestOptions options, RequestInterceptorHandler handler) {
      // Do something before the request is sent
      if (isAuth) {
        TokenDTO token = TokenDTO.fromJson(mapKey['data']);
        options.headers['Authorization'] = token.refresh_token;
      }
      options.contentType = Headers.jsonContentType;
      return handler
          .next(options); // Pass the request on to the next interceptor
    },
    onResponse: (Response response, ResponseInterceptorHandler handler) {
      if (response.statusCode == 403) {
          appState.isLogined = getx.RxBool(false);
      }
      if (response.statusCode == 401) {
        if(mapKey != null){
          TokenDTO token = TokenDTO.fromJson(mapKey['data']);

          authService.refreshToken(token.refresh_token).then((response){
              box.write('appKey', response);
          }).cathError((e){
              print(e);
              appState.isLogined = getx.RxBool(false);
          });


        }
      }
      return handler
          .next(response); // Pass the response on to the next interceptor
    },
    onError: (DioError error, ErrorInterceptorHandler handler) {
      // Do something with the error
      return handler.next(error); // Pass the error on to the next interceptor
    },
  ));

  return dio;
}

class BaseService<T> {
  late String baseUrl =
      dotenv.env['BACK_END_URL'] ?? "http://jsonplaceholder.typicode.com";
  BaseService();
  Dio dio = getDioInstance(false);
  Dio dioAuth = getDioInstance(true);

  Future<T> getDetail(
      {required String url,
      bool isAuth = false,
      required T Function(dynamic json) fromJson,
      Map<String, dynamic>? queryParams}) async {
    var dioFinal = isAuth ? dioAuth : dio;
    var response =
        await dioFinal.get("${this.baseUrl}/$url", queryParameters: queryParams);
    if (response.statusCode == 200) {
      final data = fromJson(response.data);
      return data;
    } else {
      throw Exception('Failed to load detail ');
    }
  }

  Future<List<T>> fetchList(
      {required String url,
      bool isAuth = false,
      required T Function(dynamic json) fromJson,
      Map<String, dynamic>? queryParams}) async {
    var dioFinal = isAuth ? dioAuth : dio;

    final response =
        await dioFinal.get("${this.baseUrl}/$url", queryParameters: queryParams);
    if (response.statusCode == 200) {
      return response.data.map<Quote>((json) => Quote.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load list ');
    }
  }

  Future<T> delete(
      {required String url,
      bool isAuth = false,
      Map<String, dynamic>? queryParams}) async {
    var dioFinal = isAuth ? dioAuth : dio;
    final response = await dioFinal.delete("${this.baseUrl}/$url");
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('Failed to load list ');
    }
  }

  Future<Map<String, dynamic>> post(
      {required String url, bool isAuth = false, dynamic data }) async {
    var dioFinal = isAuth ? dioAuth : dio;
    final response = await dioFinal.post("${this.baseUrl}/$url", data: data);
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('Failed to create ');
    }
  }

  Future<T> put(
      {required String url, bool isAuth = false, dynamic data}) async {
    var dioFinal = isAuth ? dioAuth : dio;
    final response = await dioFinal.put("${this.baseUrl}/$url", data: data);
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('Failed to update');
    }
  }
}

BaseService baseService = BaseService();
