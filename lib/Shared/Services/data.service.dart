import 'package:dio/dio.dart';
import 'package:erp_connect_admin/Shared/Models/Data.model.dart';
import 'package:erp_connect_admin/Shared/Models/_example.model.dart';
import 'package:erp_connect_admin/Shared/Services/base.service.dart';
import 'package:erp_connect_admin/Shared/Utils/Constant.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_storage/get_storage.dart';

class DataService {
  final box = GetStorage();
  final baseService = BaseService<DataDTO>();
  Dio dioAuth = getDioInstance(true);
  late String baseUrl = dotenv.env['BACK_END_URL'] ?? 'localhost:3000';

  Future<List<DataDTO>> getDataDynamic(
      {required String bank,
      required String type, //"transaction" | "customer" | "account"
      Map<String, dynamic>? query}) async {
    final response = await dioAuth.post("${baseUrl}/${bank}/list-${type}",
        data: {"bank": "mb", "page": 0, "size": 2, "filters": []});
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('Failed to getDataDynamic');
    }
  }
}

DataService dataService = DataService();
