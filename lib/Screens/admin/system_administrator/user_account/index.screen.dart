import 'package:erp_connect_admin/Shared/Widgets/pagination.widget.dart';
import 'package:erp_connect_admin/Shared/Layout/dashboard.layout.dart';
import 'package:erp_connect_admin/Shared/Models/_example.model.dart';
import 'package:erp_connect_admin/Shared/Services/QuoteService.dart';
import 'package:flutter/material.dart';

class UserAccount extends StatefulWidget {
  const UserAccount({Key? key}) : super(key: key);

  @override
  State<UserAccount> createState() => _UserAccountState();
}

class _UserAccountState extends State<UserAccount> {
  late Future<List<Quote>> futureQuoteList;
  int selectedPage = 1;
  void changePage(int page) {
    setState(() {
      selectedPage = page;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    futureQuoteList = quoteService.getList(selectedPage);

    return DashboardLayout(
      currentIndex: 2,
      nameScreen: "Data Page",
      body: ListView(
        // padding: const EdgeInsets.all(16),
        children: [
          FutureBuilder<List<Quote>>(
              future: futureQuoteList,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 16, top: 16),
                        child: const Text(
                          'User Account',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      const SizedBox(height: 10),
                      DataTable(
                          columns: const [
                            DataColumn(label: Text('Id')),
                            DataColumn(label: Text('UserId')),
                            DataColumn(label: Text('Title')),
                          ],
                          rows: snapshot.data!
                              .map((e) => DataRow(
                                    cells: <DataCell>[
                                      DataCell(Text(e.id.toString())),
                                      DataCell(Text(e.userId.toString())),
                                      DataCell(Text(e.title)),
                                    ],
                                  ))
                              .toList()

                          // source: _DataSource(context, snapshot.data!),
                          ),
                      const SizedBox(height: 10),
                      AppPagination(
                        selectedPage: selectedPage,
                        setStateFn: changePage,
                      )
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return const Center(child: CircularProgressIndicator());
              })
        ],
      ),
    );
  }
}
