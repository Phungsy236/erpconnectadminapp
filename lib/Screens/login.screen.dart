import 'package:erp_connect_admin/Shared/AppStates/AppController.dart';
import 'package:erp_connect_admin/Shared/Services/authen.service.dart';
import 'package:erp_connect_admin/Shared/Utils/Constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final appState = Get.find<AppController>();
  late final TextEditingController _username = TextEditingController();
  late final TextEditingController _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final box = GetStorage();
  @override
  void initState() {
    _username.text = '';
    _password.text = '';
    super.initState();
  }

  @override
  void dispose() {
    _username.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: APP_COLORS.tdBGColor,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.white.withOpacity(0.6), BlendMode.dstATop),
              image: const AssetImage("assets/images/ngsLogo.png")),
        ),
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
        child: Center(
          child: SingleChildScrollView(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              const Padding(
                padding:
                    EdgeInsets.only(left: 15, bottom: 20, right: 20, top: 10),
                child: Text(
                  "Welcome to ERPConnect Admin",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                ),
              ),
              Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: _username,
                        decoration: const InputDecoration(
                            hintText: "Username",
                            suffixIcon: Icon(Icons.people_outlined)),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter username';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _password,
                        decoration: const InputDecoration(
                            hintText: "Password",
                            suffixIcon: Icon(Icons.lock_outlined)),
                        obscureText: true,
                        enableSuggestions: false,
                        autocorrect: false,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter password';
                          }
                          return null;
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: ElevatedButton(
                            onPressed: () async {
                              final username = _username.text;
                              final password = _password.text;
                              if (_formKey.currentState!.validate()) {
                                authService
                                    .authenticated({
                                      'username': username,
                                      'password': password,
                                    })
                                    .then((response) => {
                                          if(response!= null){
                                            box.write("appKey", response)
                                          },
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            const SnackBar(
                                                content:
                                                    Text('Login successful!')),
                                          ),
                                          appState.login(),
                                        })
                                    .catchError((error) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                            content: Text('Login failed!')),
                                      );
                                      print(error);
                                    });
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                fixedSize: const Size(240, 40)),
                            child: const Text(
                              "Sign in",
                              style: TextStyle(fontSize: 16),
                            )),
                      ),
                    ],
                  )),
            ]),
          ),
        ),
      ),
    );
  }
}
