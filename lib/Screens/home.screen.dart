import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:erp_connect_admin/Shared/Widgets/customTag.wiget.dart';
import 'package:erp_connect_admin/Shared/Widgets/imageContainer.widget.dart';
import 'package:flutter/material.dart';
import 'package:erp_connect_admin/Shared/Layout/dashboard.layout.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DashboardLayout(
        currentIndex: 1,
        nameScreen: "Home Page",
        body: ListView(
          padding: EdgeInsets.zero,
          children: [
            _NewsOfTheDay(),
            _BreakingNews(),
            TextButton(
              onPressed: () {
                AwesomeNotifications().createNotification(
                    content: NotificationContent(
                        id: 10,
                        channelKey: "basic_channel",
                        title: "Test",
                        body: "Body Test123"));
              },
              child: const Text("Test"),
            )
          ],
        ));
  }
}

class _BreakingNews extends StatelessWidget {
  const _BreakingNews({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Breaking News',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              Text('More', style: Theme.of(context).textTheme.bodyLarge!),
            ],
          ),
          const SizedBox(height: 20),
          SizedBox(
            height: 250,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 2,
                itemBuilder: ((context, index) {
                  return Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    margin: const EdgeInsets.only(right: 10),
                    child: InkWell(
                      onTap: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ImageContainer(
                              width: MediaQuery.of(context).size.width * 0.5,
                              imageUrl:
                                  "http://ngs.com.vn/images/magazine-1.png"),
                          const SizedBox(height: 10),
                          Text(
                            'News Old',
                            maxLines: 2,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(
                                    fontWeight: FontWeight.bold, height: 1.5),
                          ),
                          const SizedBox(height: 5),
                          Text('${DateTime.now()}',
                              style: Theme.of(context).textTheme.bodySmall),
                          const SizedBox(height: 5),
                          Text('by Thuan.Vu',
                              style: Theme.of(context).textTheme.bodySmall),
                        ],
                      ),
                    ),
                  );
                })),
          )
        ],
      ),
    );
  }
}

class _NewsOfTheDay extends StatelessWidget {
  const _NewsOfTheDay({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ImageContainer(
      height: MediaQuery.of(context).size.height * 0.45,
      width: double.infinity,
      imageUrl: "http://ngs.com.vn/images/magazine-2.png",
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomTag(backgroundColor: Colors.grey.withAlpha(150), children: [
            Text(
              'News of the Day',
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(color: Colors.white),
            ),
          ]),
          const SizedBox(height: 10),
          Text('Lorum',
              style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                  color: Colors.white,
                  height: 1.25,
                  fontWeight: FontWeight.bold)),
          TextButton(
              onPressed: () {},
              style: TextButton.styleFrom(padding: EdgeInsets.zero),
              child: Row(
                children: [
                  Text('Learn more',
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            color: Colors.white,
                          )),
                  const SizedBox(
                    width: 10,
                  ),
                  const Icon(Icons.arrow_right_alt, color: Colors.white)
                ],
              )),
        ],
      ),
    );
  }
}
