import 'package:erp_connect_admin/Shared/Models/_example.model.dart';
import 'package:erp_connect_admin/Shared/Services/QuoteService.dart';
import 'package:flutter/material.dart';

class ExampleCallApi extends StatefulWidget {
  const ExampleCallApi({Key? key}) : super(key: key);

  @override
  State<ExampleCallApi> createState() => _ExampleCallApiState();
}

class _ExampleCallApiState extends State<ExampleCallApi> {
  late Future<Quote> futureAlbum;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureAlbum = quoteService.getDetail();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
          child: FutureBuilder<Quote>(
        future: futureAlbum,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            print(snapshot.data);
            return Text(snapshot.data!.title);
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          // By default, show a loading spinner.
          return const CircularProgressIndicator();
        },
      )),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        elevation: 0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.favorite),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
