import 'package:erp_connect_admin/Shared/Widgets/pagination.widget.dart';
import 'package:erp_connect_admin/Shared/Layout/dashboard.layout.dart';
import 'package:erp_connect_admin/Shared/Models/Data.model.dart';
import 'package:erp_connect_admin/Shared/Models/_example.model.dart';
import 'package:erp_connect_admin/Shared/Services/QuoteService.dart';
import 'package:erp_connect_admin/Shared/Services/data.service.dart';
import 'package:flutter/material.dart';
import 'package:pagination_flutter/pagination.dart';

class BankData extends StatefulWidget {
  const BankData({Key? key}) : super(key: key);

  @override
  State<BankData> createState() => _BankDataState();
}

class _BankDataState extends State<BankData> {
  late Future<List<Quote>> futureQuoteList;
  int selectedPage = 1;
  void changePage(int page){
    setState(() {
      selectedPage = page;
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final dataList = dataService.getDataDynamic(bank: 'MB', type: "transaction");

    return DashboardLayout(nameScreen: "Bank Data" , currentIndex: 2, body:
    ListView(
      // padding: const EdgeInsets.all(16),
      children: [
        FutureBuilder<List<DataDTO>>(
            future: dataList,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                // print(snapshot.data);
                return Column(
                  children: [
                    DataTable(
                        columns: [
                          DataColumn(label: Text('requestId')),
                          DataColumn(label: Text('dateAndTime')),
                          DataColumn(label: Text('type')),
                          DataColumn(label: Text('content')),
                          DataColumn(label: Text('status')),
                        ],
                        rows:  snapshot.data!.map((e) => DataRow(
                          cells: <DataCell>[
                            DataCell(Text(e.requestId.toString())),
                            DataCell(Text(e.dateAndTime.toString())),
                            DataCell(Text(e.type.toString())),
                            DataCell(Text(e.content.toString())),
                            DataCell(Text(e.errorcode =="0" ? "Active": "Error")),
                          ],
                        )).toList()

                      // source: _DataSource(context, snapshot.data!),
                    ),
                    AppPagination(selectedPage: selectedPage , setStateFn: changePage,)
                  ],
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return const CircularProgressIndicator();
            })
      ],
    ),
    );
  }
}

