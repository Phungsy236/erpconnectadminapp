import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:erp_connect_admin/Shared/Layout/dashboard.layout.dart';
import 'package:erp_connect_admin/Shared/Utils/Constant.dart';

class SettingPage extends StatefulWidget {
  @override
  State<SettingPage> createState() => _SettingPage();
}

class _SettingPage extends State<SettingPage> {
  bool _isDark = false;
  bool _isNotificated = true;

  @override
  Widget build(BuildContext context) {
    return DashboardLayout(
        currentIndex: 1,
        nameScreen: "Setting Page",
        body: Container(
            padding: const EdgeInsets.all(8.0),
            child: TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      APP_ROUTES.error, (route) => false);
                },
                child: ListView(
                  children: [
                    ListTile(
                      title: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Thuan Vu",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(
                                            fontWeight: FontWeight.bold,
                                            height: 1.5)),
                                Text("particular since July,7 2003",
                                    style:
                                        Theme.of(context).textTheme.bodySmall!)
                              ],
                            )
                          ]),
                      leading: ClipRRect(
                        borderRadius: BorderRadius.circular(50.0),
                        child: Image.asset(
                          "assets/images/userIcon.png",
                        ),
                      ),
                      trailing: const Icon(Icons.edit_outlined),
                      onTap: () {},
                    ),
                    const SizedBox(height: 20),
                    Text(
                      "Setting",
                      style: Theme.of(context)
                          .textTheme
                          .bodyLarge!
                          .copyWith(fontWeight: FontWeight.bold, height: 1.5),
                    ),
                    _CustomListTile(
                        title: "Dark Mode",
                        icon: Icons.dark_mode_outlined,
                        trailing: Switch(
                            value: _isDark,
                            onChanged: (value) {
                              setState(() {
                                _isDark = value;
                              });
                            })),
                    _CustomListTile(
                        title: "Notifications",
                        icon: Icons.notifications_none_rounded,
                        trailing: Switch(
                            value: _isNotificated,
                            onChanged: (value) {
                              setState(() {
                                _isNotificated = value;
                              });
                            })),
                    const _CustomListTile(
                        title: "Logout", icon: Icons.logout_outlined)
                  ],
                ))));
  }
}

class _CustomListTile extends StatelessWidget {
  final String title;
  final IconData icon;
  final Widget? trailing;
  const _CustomListTile({
    Key? key,
    required this.title,
    required this.icon,
    this.trailing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title),
      leading: Icon(icon),
      trailing: trailing,
      onTap: () {},
    );
  }
}
