import 'package:erp_connect_admin/Shared/Utils/Constant.dart';
import 'package:flutter/material.dart';

class FileNotFoundDark extends StatelessWidget {
  const FileNotFoundDark({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            'assets/images/file_not_found_second.png',
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
          const Positioned(
            bottom: 200,
            left: 30,
            child:
                Text('File Not Found', style: TextStyle(color: Colors.white54)),
          ),
          const Positioned(
            bottom: 140,
            left: 30,
            child: Text(
              'Oops! The file you are looking for\nis not found',
              style: TextStyle(
                color: Colors.white54,
              ),
              textAlign: TextAlign.start,
            ),
          ),
          Positioned(
            bottom: 70,
            left: 30,
            right: 250,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white,
                foregroundColor: Colors.black,
              ),
              onPressed: () {
                Navigator.pushNamed(context, APP_ROUTES.home);
              },
              child: const Text(
                "Home",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
