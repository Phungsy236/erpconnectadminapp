import 'package:erp_connect_admin/Screens/401.screen.dart';
import 'package:erp_connect_admin/Screens/admin/system_administrator/user_account/index.screen.dart';
import 'package:erp_connect_admin/Screens/data/bank.screen.dart';
import 'package:erp_connect_admin/Screens/home.screen.dart';
import 'package:erp_connect_admin/Screens/login.screen.dart';
import 'package:erp_connect_admin/Screens/setting.screen.dart';
import 'package:erp_connect_admin/Shared/AppStates/AppController.dart';
import 'package:erp_connect_admin/Shared/Configs/ThemeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:awesome_notifications/awesome_notifications.dart';

void main() async {
  try {
    await Future.delayed(const Duration(seconds: 1));
    await GetStorage.init();
    await dotenv.load(fileName: ".env");
  } catch (e) {
    throw ("An error occurred: $e");
  }
  AwesomeNotifications().initialize(
      null,
      [
        NotificationChannel(
          channelKey: 'basic_channel',
          channelName: 'Basic notifications',
          channelDescription: 'Notification channel for basic tests',
        )
      ],
      debug: true);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static var navigatorKey;

  MyApp({super.key});
  final appState = Get.put(AppController());
  @override
  Widget build(BuildContext context) {
    appState.checkToken();
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return GetMaterialApp(
      navigatorKey: MyApp.navigatorKey,
      debugShowCheckedModeBanner: false,
      title: 'ErpConnect App',
      theme: DAppTheme.lightTheme,
      darkTheme: DAppTheme.darkTheme,
      themeMode: ThemeMode.system,
      // theme: ThemeData(
      //   primarySwatch: Colors.blue,
      // ),
      // home: const ExampleCallApi()
      home: Obx(() => appState.isLogined.value == false
          ? const HomePage()
          : const LoginPage()),
      getPages: [
        GetPage(name: "/error", page: () => const FileNotFoundDark()),
        GetPage(name: "/home", page: () => const HomePage()),
        GetPage(name: "/login", page: () => const LoginPage()),
        GetPage(name: "/home", page: () => const HomePage()),
        GetPage(name: "/setting", page: () => SettingPage()),
        GetPage(
          name: "/bidv/transaction",
          page: () => const HomePage(),
        ),
        GetPage(
            name: "/admin/system_administrator/user_account",
            page: () => const UserAccount()),
      ],
    );
  }
}
